
var WaterPipeline = new Phaser.Class({

    Extends: Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline,

    initialize:

    function DistortPipeline (game)
    {
        Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline.call(this, {
            game: game,
            renderer: game.renderer,
            fragShader: `
            precision mediump float;
            uniform float     time;
            uniform vec2      resolution;
            uniform sampler2D uMainSampler;
            varying vec2 outTexCoord;
            void main( void ) {
                vec4 color = texture2D(uMainSampler, outTexCoord);
                vec2 uv = outTexCoord;
                float swing = sin((uv.x + sin(uv.y * 10.0 + time * 10.0) * 0.03) * 32.0);
                vec4 wColor = vec4(color.r * (1.0 + swing * sin((uv.y - time * 0.35) * 128.0)*0.1), color.g * (1.0 + swing * sin((uv.y - time * 0.35) * 128.0)*0.1), color.b, 1.0);
                gl_FragColor = wColor;
            }`
        });
    } 

});